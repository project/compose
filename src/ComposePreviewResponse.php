<?php

namespace Drupal\compose;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Render\AttachmentsTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * JSON response object for AJAX requests.
 *
 * @ingroup ajax
 */
class ComposePreviewResponse extends JsonResponse implements AttachmentsInterface {
  use AttachmentsTrait;

  /**
   * The markup for the response as a string.
   *
   * @var string
   */
  protected $markup = '';

  /**
   * Sets the HTML content for the response.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface $markup
   *   The markup as a string or renderable array.
   */
  public function setMarkup($markup) {
    $this->markup = $markup;
  }

  /**
   * Returns the markup.
   *
   * @return string|\Drupal\Component\Render\MarkupInterface
   *   The markup.
   */
  public function getMarkup() {
    return $this->markup;
  }

}
