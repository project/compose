<?php

namespace Drupal\compose;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Entity\Query\QueryInterface;

/**
 * Generic entity handler for compose widget functions.
 */
class EntityComposeWidget implements ComposeWidgetInterface {

  protected $entityTypeManager;
  protected $entityFieldManager;
  protected $bundleInfo;
  protected $entityType;

  /**
   * Constructs the compose widget controller.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function __construct(
      EntityFieldManagerInterface $entity_field_manager,
      EntityTypeManagerInterface $entity_type_manager,
      EntityTypeBundleInfoInterface $bundle_info,
      EntityTypeInterface $entity_type
    ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityType = $entity_type;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $entity_type
    );
  }

  /**
   * {@inheritdoc}
   */
  public function addNew($bundle_id) {
    // Get entity type info.
    $bundle = $this->entityTypeManager->getStorage($this->entityType->getBundleEntityType())->load($bundle_id);
    $bundle_label = $bundle->label();
    // Get entity type field info.
    if ($fields = $this->entityFieldManager->getFieldDefinitions($this->entityType->id(), $bundle_id)) {
      $field_labels = [];
      foreach ($fields as $field_key => $field) {
        if (strpos($field_key, 'field') !== FALSE) {
          $field_labels[] = $field->getLabel();
        }
      }
      $available_fields = 'Fields: ' . implode(', ', $field_labels);
    }

    // Build Add New Assebly rows
    $row = [
      '#type' => 'container',
      '#attributes' => ['class' => ['add-new-row']],
    ];
    $row['info'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['add-new-right']],
      '#weight' => 1,
    ];
    $row['info']['text'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['info-text']],
    ];
    $row['info']['text']['bundle_id'] = [
      '#type' => 'hidden',
      '#value' => $bundle_id,
    ];
    $row['info']['text']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $bundle_label,
    ];
    if (!empty($bundle->description)) {
      $row['info']['text']['description'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $bundle->description,
        '#attributes' => ['class' => ['light-text']],
      ];
    }
    if (isset($available_fields)) {
      $row['info']['text']['fields'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $available_fields,
        '#attributes' => ['class' => ['add-new-fields']],
      ];
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function addNewSelectButton(array &$row, array &$button) {
    $row['info']['button'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['info-button']],
    ];
    $row['info']['button']['actions']['submit'] = $button;
  }

  /**
   * {@inheritdoc}
   */
  public function addExistingRow(EntityInterface $entity) {
    $bundle_info = $this->bundleInfo->getBundleInfo($entity->getEntityTypeId());
    $row = [
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $entity->label(),
      ],
      'type' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $bundle_info[$entity->bundle()]['label'],
      ]
    ];

    $entity_types = \Drupal::moduleHandler()->invokeAll('compose_add_existing_row_data');
    foreach ($entity_types as $entity_type => $data) {
      if ($entity_type == $entity->getEntityTypeId()) {
        foreach ($data as $key => $value) {
          $row[$key] = call_user_func($value['callback'], $entity);
        }
      }
    }
      
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function getAddExistingFilters(array $bundles = []) {
    $bundle_info = $this->bundleInfo->getBundleInfo($this->entityType->id());
    $bundle_key = $this->entityType->getKey('bundle');

    return [
      'bundle' => [
        'label' => $this->entityType->getBundleLabel(),
        'field' => $bundle_key,
        'operator' => '=',
        'options' => array_map(function($val) {
          return $val['label'];
        }, array_intersect_key($bundle_info, array_flip($bundles)))
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getAddExistingSort(QueryInterface $query) {
    return $query;
  }
}
