<?php

namespace Drupal\compose\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AjaxResponseAttachmentsProcessor;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormState;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Theme\ThemeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\compose\EntityPreviewLoader;
use Drupal\compose\ComposePreviewResponse;
use Drupal\compose\ComposePreviewResponseAttachmentsProcessor;

/**
 * Class EntityPreviewController.
 */
class EntityPreviewController extends ControllerBase {

  private $entityPreviewLoader;

  private $entityTypeManger;

  private $renderer;

  private $themeManager;

  private $attachmentsProcessor;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityPreviewLoader $entity_preview_loader, EntityTypeManager $entity_type_manager, Renderer $renderer, ThemeManager $theme_manager, ComposePreviewResponseAttachmentsProcessor $attachments_processor) {
    $this->entityPreviewLoader = $entity_preview_loader;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->themeManager = $theme_manager;
    $this->attachmentsProcessor = $attachments_processor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entityPreviewLoader = $container->get('compose.entity_preview_loader');
    $entityTypeManager = $container->get('entity_type.manager');
    $renderer = $container->get('renderer');
    $themeManager = $container->get('theme.manager');
    $attachmentsProcessor = $container->get('compose.preview_attachments_processor');

    return new static(
      $entityPreviewLoader,
      $entityTypeManager,
      $renderer,
      $themeManager,
      $attachmentsProcessor
    );
  }

  /**
   * Returns the markup of the entity with $entity_id.
   *
   * @param string $form_build_id
   *   The build ID of the form.
   * @param string $field_name
   *   The field name of the field using the ComposeWidget.
   * @param string $delta
   *   The delta of this specific reference by the field.
   * @param string $entity_type
   *   The entity type of the host entity of this field.
   * @param string $entity_id
   *   (optional) The ID of the entity. An ID will not exist for entities that
   *   are currently being created.
   * @param request \Drupal
   *
   * @return string
   *   The rendered markup string of the entity.
   */
  public function render($form_build_id, $field_name, $delta, $entity_type, $entity_id) {
    //Get the preview entity
    $entity = $this->entityPreviewLoader->getPreviewEntity($form_build_id, $field_name, $delta, $entity_type, $entity_id);

    // Get the ViewBuilder for this entity type.
    $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
    $entity_render_array = $view_builder->view($entity);
    $entity_markup = $this->renderer->renderRoot($entity_render_array);

    // Get the libraries from the active theme (RHDP).
    // @see Drupal\compose\Theme\ThemeNegotiator
    $libraries = $this->themeManager->getActiveTheme()->getLibraries();
    // Create a new AjaxResponse object and append our markup to our Compose
    // entity widget.
    $response = new ComposePreviewResponse();
    $response->setMarkup($entity_markup);

    // Add the proper Libraries to the attachments of this AjaxResponse.
    $response = $response->addAttachments(['library' => $libraries]);
    // Process the attachments of this AjaxResponse.
    $response = $this->attachmentsProcessor->processAttachments($response);

    return $response;
  }

}
