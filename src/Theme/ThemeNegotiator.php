<?php

namespace Drupal\compose\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Routing\RouteMatchInterface;

class ThemeNegotiator implements ThemeNegotiatorInterface {
  /**
   * ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactoryInterface;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory_interface
   *   ThemeManager.
   */
  public function __construct(ConfigFactoryInterface $config_factory_interface) {
    $this->configFactoryInterface = $config_factory_interface;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'compose.entity_preview_controller_render';
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->configFactoryInterface->get('system.theme')->get('default');
  }

}
