<?php
namespace Drupal\compose\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Form\FormState;
use Drupal\compose\EntityPreviewLoader;

/**
 * Checks access for displaying configuration translation page.
 */
class EntityPreviewAccessCheck implements AccessInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\compose\EntityPreviewLoader
   */
  private $entityPreviewLoader;

  /**
   * Constructs an EntityCreateAccessCheck object.
   *
   * @param \Drupal\compose\EntityPreviewLoader $entity_preview_loader
   *   The entity preview loader service.
   */
  public function __construct(EntityPreviewLoader $entity_preview_loader) {
    $this->entityPreviewLoader = $entity_preview_loader;
  }

  /**
   * Checks access to the entity preview page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param string $form_build_id
   *   The build ID of the form.
   * @param string $field_name
   *   The field name of the field using the ComposeWidget.
   * @param string $delta
   *   The delta of this specific reference by the field.
   * @param string $entity_type
   *   The entity type of the host entity of this field.
   * @param string $entity_id
   *   (optional) The ID of the entity. An ID will not exist for entities that
   *   are currently being created.
   *
   * @return string
   *   A \Drupal\Core\Access\AccessResult constant value.
   */
  public function access(AccountInterface $account, $form_build_id, $field_name, $delta, $entity_type, $entity_id) {
    if ($entity = $this->entityPreviewLoader->getPreviewEntity($form_build_id, $field_name, $delta, $entity_type, $entity_id)) {
      return $entity->access('update', $account, TRUE);
    }
    else {
      return AccessResult::neutral();
    }
  }

}
