<?php

namespace Drupal\compose\Ajax;

use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Asset\AttachedAssets;

/**
 * Class ComposeOpenModalDialog.
 */
class ComposeOpenModalDialog extends OpenDialogCommand {

  /**
   * Render custom ajax command.
   *
   * @param string $selector
   *   The selector for the dialog content.
   * @param string $title
   *   The title for the dialog.
   * @param string $container
   *   The selector for the dialog container.
   * @param string $append
   *   The selector for the element to which the dialog is appended. Required
   *   for jquery ui dialog.
   * @param array $classes
   *   Classes to be added to the dialog container.
   */
  public function __construct($selector, $title, $container, $append, array $classes = []) {
    $settings = NULL;
    $this->container = $container;
    $dialog_options = [
      'modal' => TRUE,
      'appendTo' => $append,
      'width' => '900',
      // 'closeOnEscape' => FALSE,
      'dialogClass' => 'compose-dialog',
      // 'backdrop' => 'static',
      'keyboard' => FALSE,
      'classes' => [
        'ui-dialog-content' => 'compose-dialog-content',
      ],
    ];
    $this->classes = $classes;

    parent::__construct($selector, $title, [], $dialog_options, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $this->attachedAssets = new AttachedAssets();

    return [
      'command' => 'composeOpenDialog',
      'selector' => $this->selector,
      'container' => $this->container,
      'settings' => $this->settings,
      'dialogOptions' => $this->dialogOptions,
      'classes' => $this->classes,
    ];
  }

}
