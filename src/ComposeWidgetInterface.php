<?php

namespace Drupal\compose;

use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Entity\Query\QueryInterface;

/**
 * Defines the interface for ComposeWidget entity handlers.
 */
interface ComposeWidgetInterface extends EntityHandlerInterface {

  /**
   * Get render array for previewing bundle when adding a new entity.
   *
   * @param string $bundle_id
   *   An array of allowed bundles for this widget.
   *
   * @return array
   *   A renderable array to be shown as the add new preview.
   */
  public function addNew($bundle_id);

  /**
   * Adds the select button to the row.
   *
   * @param array $row
   *   The renderable row as returned by addNew.
   * @param array $button
   *   The renderable select button code to be added to the row.
   */
  public function addNewSelectButton(array &$row, array &$button);

  /**
   * Get render array for form/table for adding an existing entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The array of parent elements.
   *
   * @return array
   *   A renderable array to be shown as the add existing form.
   */
  public function addExistingRow(EntityInterface $entity);

  /**
   * Get the list of filters available when adding existing items.
   *
   * @param array $bundles
   *   The list of allowed bundles for this entity.
   *
   * @return array
   *   A list of filters available as an array. Each item in the array contains:
   *     * The label for the filter
   *     * The field used to add to the entity query
   *     * The operator for the filter
   *     * A list of options if the field should be presented as a selected,
   *       keyed as value => label
   */
  public function getAddExistingFilters(array $bundles = []);

  /**
   * Sort the Add Existing table.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   Drupal EntityQuery
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given entity type.
   */
  public function getAddExistingSort(QueryInterface $query);

}
