(function ($, Drupal) {
Drupal.AjaxCommands.prototype.composeOpenDialog = function (ajax, response, status) {

  // make sure jquery ui dialogs don't tank ckeditor dialogs
  orig_allowInteraction = $.ui.dialog.prototype._allowInteraction;
  $.ui.dialog.prototype._allowInteraction = function (event) {
    if ($(event.target).closest('.cke_dialog').length) {
      return true;
    }
    return orig_allowInteraction.apply(this, arguments);
  };

  if (!response.selector) {
    return false;
  }

  var $content = $(response.selector);
  var $dialog = $(response.container);
  $dialog.append($content);
  if (response.classes) {
    $dialog.addClass(response.classes.join(' '));
    $dialog.data('compose-classes', response.classes);
  }

  if (!ajax.wrapper) {
    ajax.wrapper = $dialog.attr('id');
  }

  if (!response.dialogOptions.buttons) {
    response.dialogOptions.drupalAutoButtons = true;
    response.dialogOptions.buttons = Drupal.behaviors.dialog.prepareDialogButtons($dialog);
  }

  $dialog.on('dialogButtonsChange', function () {
    var buttons = Drupal.behaviors.dialog.prepareDialogButtons($dialog);
    $dialog.dialog('option', 'buttons', buttons);
  });

  response.dialogOptions = response.dialogOptions || {};
  var dialog = Drupal.dialog($dialog.get(0), response.dialogOptions);
  // handle some bootstrap modal stuff
  if (response.dialogOptions.modal) {
    dialog.showModal();
  } else {
    dialog.show();
  }

  $dialog.on('hide.bs.modal', function(evt) {
    for (ajax in Drupal.ajax.instances) {
      try {
        if (Drupal.ajax.instances[ajax].ajaxing && $(this).find(Drupal.ajax.instances[ajax].selector).length > 0) {
          return;
        }
      }
      catch (e) { }
    }
    $cancel = $(this).find('[data-compose-cancel]');
    if ($cancel.length > 0 && !$(this).data('compose-dialog-closing')) {
      $(this).data('compose-dialog-closing', true);
      evt.stopPropagation()
      evt.preventDefault();
      $cancel.mousedown();
      return false;
    }
  });
  $dialog.on('hidden.bs.modal', function(evt) {
    var classes = $dialog.data('compose-classes');
    if (classes) {
      $dialog.removeClass(classes.join(' '));
    }
  });
  $dialog.parent().find('.ui-dialog-buttonset').addClass('form-actions');


}

Drupal.behaviors.composeFilters = {
  attach: function(context, settings) {
    $('.compose-filters').once('composeFilters').each(function() {
      var $this = $(this), $submit = $this.find('.compose-filter-actions :input[type=submit]').first();
      $this.find(':input:not([type=submit])').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
          e.preventDefault();
          $submit.trigger('mousedown');
          return false;
        }
      })
    });
  }
}

})(jQuery, Drupal);
